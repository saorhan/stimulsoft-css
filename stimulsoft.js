StiJsViewer.prototype.InitializeViewerControls = function () {
    var jsObject = this;

    this.collections.images['Print.png'] = '/images/solid-print.png';
    this.collections.images['PrevPage.png'] = '/images/line-left-arrow.png';
    this.collections.images['NextPage.png'] = '/images/line-right-arrow.png';
    this.collections.images['PageFirst20.png'] = '/images/line-left-double-arrow.png';
    this.collections.images['PageNext20.png'] = '/images/line-right-arrow.png';
    this.collections.images['PagePrevious20.png'] = '/images/line-left-arrow.png';
    this.collections.images['PageLast20.png'] = '/images/line-right-double-arrow.png';
    this.collections.images['Arrows.SmallArrowDown.png'] = '/images/line-down-arrow.png';
    this.collections.images['Arrows.SmallArrowUp.png'] = '/images/line-up-arrow.png';
    this.collections.images['SendEmail.png'] = '/images/solid-mail.png';
    this.collections.images['Save.png'] = '/images/solid-save.png';
    this.collections.images['ViewMode.png'] = '/images/solid-file-preview.png';
    this.collections.images['Save.Small.Pdf.png'] = '/images/solid-file-pdf.png';
    this.collections.images['Save.Small.Data.png'] = '/images/solid-file-data.png';
    this.collections.images['Save.Small.Html.png'] = '/images/solid-file-html.png';
    this.collections.images['Save.Small.Excel.png'] = '/images/solid-file-xls.png';
    this.collections.images['Save.Small.Image.png'] = '/images/solid-file-image.png';

    if (this.options.isMobileDevice) this.InitializeMobile();
    else this.options.toolbar.showPinToolbarButton = false;
    
    this.InitializeJsViewer();
    this.InitializeDashboardsPanel();
    this.InitializeToolBar();
    if (this.options.toolbar.showFindButton) this.InitializeFindPanel();
    this.InitializeDrillDownPanel();
    if (this.options.toolbar.showResourcesButton) this.InitializeResourcesPanel();
    this.InitializeDisabledPanels();
    this.InitializeAboutPanel();
    this.InitializeReportPanel();
    this.InitializeProcessImage();
    this.InitializeDatePicker();
    this.InitializeToolTip();

    if (this.options.toolbar.displayMode == "Separated" && this.options.toolbar.visible) this.InitializeNavigatePanel();
    if (this.options.toolbar.showSaveButton && this.options.toolbar.visible) this.InitializeSaveMenu();
    if (this.options.toolbar.showSendEmailButton && this.options.toolbar.visible) this.InitializeSendEmailMenu();
    if (this.options.toolbar.showPrintButton && this.options.toolbar.visible) this.InitializePrintMenu();
    if (this.options.toolbar.showZoomButton && (this.options.toolbar.visible || this.options.toolbar.displayMode == "Separated")) this.InitializeZoomMenu();
    if (this.options.toolbar.showViewModeButton && this.options.toolbar.visible) this.InitializeViewModeMenu();
    if (this.options.exports.showExportDialog || this.options.email.showExportDialog) this.InitializeExportForm();
    if (this.options.toolbar.showSendEmailButton && this.options.email.showEmailDialog && this.options.toolbar.visible) this.InitializeSendEmailForm();
    this.addHoverEventsToMenus();

    this.InitializeEvents();

    if (this.options.serverMode) this.InitializeFolderReportsPanel();

    this.addEvent(document, 'mouseup', function (event) {
        jsObject.DocumentMouseUp(event)
    });

    this.addEvent(document, 'mousemove', function (event) {
        jsObject.DocumentMouseMove(event)
    });

    if (document.all && !document.querySelector) {
        alert("Your web browser is not supported by our application. Please upgrade your browser!");
    }

    console.log('this.collections', this.collections)

    this.controls.viewer.style.top = 0;
    this.controls.viewer.style.right = 0;
    this.controls.viewer.style.bottom = 0;
    this.controls.viewer.style.left = 0;
    this.changeFullScreenMode(this.options.appearance.fullScreenMode);

    if (this.onready) this.onready();
}


StiJsViewer.prototype.InitializeZoomMenu = function () {
    var items = [];
    var zoomItems = ["25", "50", "75", "100"];
    for (var i = 0; i < zoomItems.length; i++) {
        items.push(this.Item("Zoom" + zoomItems[i], zoomItems[i] + "%", "SelectedItem.png", "Zoom" + zoomItems[i]));
    }


    if (this.options.toolbar.displayMode != "Separated") {
        items.push("separator1");
        items.push(this.Item("ZoomOnePage", this.collections.loc["ZoomOnePage"], "ZoomOnePage.png", "ZoomOnePage"));
        items.push(this.Item("ZoomPageWidth", this.collections.loc["ZoomPageWidth"], "ZoomPageWidth.png", "ZoomPageWidth"));
    }

    var zoomMenu = this.VerticalMenu("zoomMenu", this.controls.toolbar.controls["Zoom"],
        this.options.toolbar.displayMode == "Separated" ? "Up" : "Down", items, null, null, this.options.toolbar.displayMode == "Separated" || this.options.appearance.rightToLeft);

    zoomMenu.action = function (menuItem) {
        zoomMenu.changeVisibleState(false);
        if (this.jsObject.options.toolbar.displayMode == "Separated") {
            this.jsObject.controls.toolbar.controls.ZoomOnePage.setSelected(false);
            this.jsObject.controls.toolbar.controls.ZoomPageWidth.setSelected(false);
        }
        zoomMenu.jsObject.postAction(menuItem.key);
    }
}

StiJsViewer.prototype.InitializeNavigatePanel = function () {
    var navigatePanel = document.createElement("div");
    navigatePanel.id = this.controls.viewer.id + "NavigatePanel";
    navigatePanel.jsObject = this;
    navigatePanel.visible = false;
    navigatePanel.style.display = "none";
    this.controls.navigatePanel = navigatePanel;
    this.controls.mainPanel.appendChild(navigatePanel);
    navigatePanel.className = "stiJsViewerNavigatePanel";
    if (this.options.isMobileDevice) {
        navigatePanel.style.transition = "margin 300ms ease, opacity 300ms ease";
        if (this.options.toolbar.autoHide) navigatePanel.style.zIndex = 5;
    }

    var controlsTable = this.CreateHTMLTable();
    navigatePanel.appendChild(controlsTable);
    
    var controlProps = [];
    controlProps.push(["Space"]);
    if (this.options.toolbar.showFirstPageButton) controlProps.push(["FirstPage", null, "PageFirst20.png", this.collections.loc["FirstPageToolTip"], null]);
    if (this.options.toolbar.showPreviousPageButton) controlProps.push(["PrevPage", null, "PagePrevious20.png", this.collections.loc["PrevPageToolTip"], null]);
    if (this.options.toolbar.showCurrentPageControl) {
        controlProps.push(["PageControl"]);
    }
    if (this.options.toolbar.showNextPageButton) controlProps.push(["NextPage", null, "PageNext20.png", this.collections.loc["NextPageToolTip"], null]);
    if (this.options.toolbar.showLastPageButton) controlProps.push(["LastPage", null, "PageLast20.png", this.collections.loc["LastPageToolTip"], null]);
    // if (this.options.toolbar.showZoomButton) {
    //     controlProps.push(["Zoom", "100%", null, this.collections.loc["Zoom"], "Up"]);
    // }

    for (var index = 0; index < controlProps.length; index++) {
        var name = controlProps[index][0];
        
        if (name.indexOf("Space") == 0) {
            controlsTable.addCell().style.width = "100%";
            continue;
        }

        if (name.indexOf("Separator") == 0) {
            controlsTable.addCell(this.NavigatePanelSeparator());
            continue;
        }

        var helpLink = this.helpLinks[name] || "user-manual/index.html?viewing_reports_basic_toolbar_of_report_viewer.htm";
        var control = (name != "PageControl")
            ? this.NavigateButton(name, controlProps[index][1], controlProps[index][2],
                (controlProps[index][3] ? [controlProps[index][3], helpLink] : null), controlProps[index][4])
            : this.PageControl();

        if (name != "PageControl") {  
            if (control.caption == null) {
                control.imageCell.style.textAlign = "center";
                control.innerTable.style.width = "100%";
                control.style.width = this.options.isMobileDevice ? "0.4in" : "35px";
            }
            if (control.toolTip) {
                var positions = { top: "isNavigatePanelTooltip" }
                if (name == "Zoom" || name == "ZoomPageWidth" || name == "ZoomOnePage") {
                    positions.rightToLeft = true;
                }
                control.toolTip.push(positions);
            }
        }
        else {
            console.log('PageControl', { controlProps, control })
            control.className += " stiJsViewerNavigatePanelPages";
            control.rows[0].removeChild(control.rows[0].cells[0]);
            control.rows[0].cells[1].innerText = "/";
            control.rows[0].cells[2].style.padding = "0px 8px";
            control.textBox.style.border = "0px";
        }
        if (control.arrow) control.arrow.src = this.collections.images["ButtonArrowUp.png"];
        if (name == "FirstPage") control.style.margin = "0 1px 0 3px";
        else if (name == "Zoom")  {
            control.className += " stiJsViewerNavigatePanelZoomSelect";
            control.style.margin = "0 3px 0 1px";
        }
        else control.style.margin = "0px 1px 0 1px";

        this.controls.toolbar.controls[name] = control;
        controlsTable.addCell(control);
    }
    
    var disabledPanel = document.createElement("div");    
    navigatePanel.disabledPanel = disabledPanel;
    disabledPanel.className = "stiJsViewerNavigatePanelDisabledPanel";
    navigatePanel.appendChild(disabledPanel);

    navigatePanel.setEnabled = function (state) {
        disabledPanel.style.display = state ? "none" : "";
    }

    navigatePanel.changeVisibleState = function (state) {
        this.visible = state;
        this.style.display = state ? "block" : "none";
    }

    navigatePanel.setEnabled(true);
    
    if (this.options.isMobileDevice) {
        this.controls.toolbar.addEventListener("touchstart", function () { navigatePanel.jsObject.controls.reportPanel.keepToolbar(); });
        navigatePanel.addEventListener("touchstart", function () { navigatePanel.jsObject.controls.reportPanel.keepToolbar(); });
        this.controls.reportPanel.showToolbar();
    }
}

//Separator
StiJsViewer.prototype.NavigatePanelSeparator = function () { return null; }

//Navigate Button
StiJsViewer.prototype.NavigateButton = function (name, caption, imageName, toolTip, arrowType) {
    var button = this.SmallButton(name, caption, imageName, toolTip, arrowType, "stiJsViewerNavigateButton");
    button.style.height = this.options.isMobileDevice ? "0.5in" : "35px";
    button.style.boxSizing = "border-box";
    if (button.arrow) button.arrow.style.marginTop = "1px";

    return button;
}
